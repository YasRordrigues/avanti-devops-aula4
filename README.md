# Desafio Avanti Bootcamp DevOps

## Sobre o Desafio

Este desafio foi proposto com o objetivo de aplicar os conhecimentos adquiridos no bootcamp Avanti DevOps, criando uma infraestrutura com Terraform na AWS, realizando deploy via GitLab CI/CD, e finalizando com o teste da aplicação hospedada em uma instância EC2.

## Processo

Os seguintes passos descrevem o processo de execução do desafio:

### 1. Criação da Infraestrutura com Terraform

Utilizamos os arquivos do Terraform desenvolvidos nas aulas anteriores para criar uma instância EC2 na AWS. O comando `terraform plan` foi usado para criar um plano de execução, e o `terraform apply` para aplicar as alterações propostas, estabelecendo a infraestrutura necessária.

![Terraform-Apply](images/terraform-apply.png)
![Terraform](images/terraform.png)

### 2. Deploy com GitLab CI/CD

Após a criação da instância, procedemos com o registro do GitLab Runner e a mudança de propriedade da pasta `/var/www/html/` para permitir que o runner execute comandos necessários para o deploy da aplicação.
![Gitlab-Runner](images/gitlab-runner.png)
![Pipeline](images/pipeline.png)

### 3. Visão Geral da Instância na AWS

A instância criada pode ser acessada diretamente através do seu endereço IP público [](http://54.82.203.166/), e o site está disponível no padrão HTTP.
![Instância AWS](images/aws.png)

## Resultados

A jornada no DevOps é refletida no site que foi criado e pode ser visualizado pela imagem abaixo:

![Site DevOps](images/site.png)

O site foi pensado para ser uma introdução amigável ao mundo do DevOps, apresentando ferramentas e práticas essenciais para quem está começando nesta área.

## Documentação e Evidências

Todo o processo foi documentado com prints do Terraform plan e apply, instâncias EC2 na AWS e a execução do pipeline no GitLab CI/CD. As evidências ajudam a validar cada etapa do desafio.

